import re
import json
import random
import decimal
import datetime

MIN_LENGTH = '1'
MAX_LENGTH = '10'
PHONE_LENGTH = '11'
PRO = '2'
DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M:%S'
ENUM_VALUE = 'demo1,demo2,demo3'
BOOL_VALUE = True


class GenTeseCases:
    """
    req_dict =
    {
        'name': '登录', # 接口名称
        'params': [     # 接口入参
        {"field": "usernmae", "attr": {"type": "1","min_length": "8", "max_length": "10"}},
        {"field": "password", "attrs": {"type": "2", "start": "1", "end": "10"}},
        {"field": "d1", "attrs": {"type": "3",
            "start": "10", "end": "100", "precision": "2"}},
        {"field": "d2", "attrs": {"type": "4", "format": "%Y -%m-%d %H:%M:%S"}},
        {"field": "d3", "attrs": {"type": "5", "phone_length": "11"}},
        {"field": "d4", "attrs": {"type": "6", "email_length": ""}},
        {"field": "d5", "attrs": {"type": "7", "enum_value": "1,2,3,4"}},
        {"field": "d6", "attrs": {"type": "8", "boolean_value": "true"}}
        ]
    }
    """

    def __init__(self, req_dict: dict):
        self.api_name = req_dict['name']
        self.req_fields = req_dict['params']
        self.unsed_fields = []
        self.field_type_list = [
            {'type_name': '字符串', 'type_code': '1'},
            {'type_name': '整数', 'type_code': '2'},
            {'type_name': '浮点数', 'type_code': '3'},
            {'type_name': '日期', 'type_code': '4'},
            {'type_name': '手机', 'type_code': '5'},
            {'type_name': '邮箱', 'type_code': '6'},
            {'type_name': '枚举', 'type_code': '7'},
            {'type_name': '布尔', 'type_code': '8'},
        ]

    def gen_testcases(self):
        self.create_value_for_field()
        testcases_list = []

        testcase_zc = []
        for uf in self.unsed_fields:
            if uf['values']['valid']:
                testcase_zc.append(
                    {'fieldName': uf['field'], 'fieldValue': uf['values']['valid']})

        zc_tc = ({'request': testcase_zc}, '%s_正常用例' % self.api_name)
        testcases_list.append(zc_tc)

        for index, unsed_field in enumerate(self.unsed_fields):
            this_field = unsed_field['field']
            this_value = unsed_field['values']
            invalids = this_value['invalid']

            for one in invalids:
                req_param = []
                content = one['content']
                describe = one['describe']
                name = '%s值为%s' % (this_field, describe)
                this_name = self.api_name + '_' + name
                req_param.append(
                    {'fieldName': this_field, 'fieldValue': content})

                for s_index, s_field in enumerate(self.unsed_fields):
                    if index == s_index:
                        continue
                    else:
                        curr_field = s_field['field']
                        curr_values = s_field['values']
                        valid = curr_values['valid']

                    req_param.append(
                        {'fieldName': curr_field, 'fieldValue': valid})

                this_test_case = ({'request': req_param}, this_name)
                testcases_list.append(this_test_case)

            if invalids:
                special_param = []

                for s_index, s_field in enumerate(self.unsed_fields):
                    if index == s_index:
                        continue
                    else:
                        curr_field = s_field['field']
                        curr_values = s_field['values']
                        valid = curr_values['valid']
                    special_param.append(
                        {'fieldName': curr_field, 'fieldValue': valid})

                special_name = '缺少%s请求参数' % this_field
                this_special_name = self.api_name + '_' + special_name
                special_testcase = (
                    {'request': special_param}, this_special_name)
                testcases_list.append(special_testcase)

        return testcases_list

    def create_value_for_field(self):
        for curr_field_info in self.req_fields:
            values = {'valid': '', 'invalid': []}

            filed = curr_field_info['field']
            attrs = curr_field_info['attrs']
            type_code = attrs.get('type', 'not_set_type')

            if type_code == '1':
                min_length = attrs.get('min_length', None)
                max_length = attrs.get('max_length', None)
                if min_length and max_length:
                    values = self._get_str_value(min_length, max_length)
                else:
                    values = self._get_str_value()
            elif type_code == '2':
                start = attrs.get('start', None)
                end = attrs.get('end', None)
                if start and end:
                    values = self._get_int_value(start, end)
                else:
                    values = self._get_int_value()
            elif type_code == '3':
                start = attrs.get('start', None)
                end = attrs.get('end', None)
                precision = attrs.get('precision', None)
                if start and end and precision:
                    values = self._get_float_value(start, end, precision)
                else:
                    values = self._get_float_value()
            elif type_code == '4':
                format_text = attrs.get('format', None)
                if format_text:
                    values = self._get_datatime_value(format_text)
                else:
                    values = self._get_datatime_value()
            elif type_code == '5':
                phone_length = attrs.get('phone_length', None)
                if phone_length:
                    values = self._get_phone_value(phone_length)
                else:
                    values = self._get_phone_value()
            elif type_code == '6':
                values = self._get_email_value()
            elif type_code == '7':
                enum_value = attrs.get('enum_value', None)
                if enum_value:
                    values = self._get_enum_value(enum_value)
                else:
                    values = self._get_enum_value()
            elif type_code == '8':
                bool_value = attrs.get('boolean_value', 'None')
                if bool_value != 'None':
                    values = self._get_boolean_value(bool_value)
                else:
                    values = self._get_boolean_value()
            else:
                pass
            self.unsed_fields.append({'field': filed, 'values': values})

    def _get_str_value(self, min_length=MIN_LENGTH, max_length=MAX_LENGTH):
        min_length = int(min_length)
        max_length = int(max_length)
        length_text = random.randint(min_length, max_length)

        seed_code = 1
        letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'
        numbers = '0123456789'
        special_character = '?._{}[]!@#$^&*()'
        describe_template = '{describe}位长度字符串'

        if seed_code == 1:
            seed = [letters]
        elif seed_code == 2:
            seed = [numbers]
        elif seed_code == 3:
            seed = [special_character]
        elif seed_code == 4:
            seed = [letters, numbers]
        elif seed_code == 5:
            seed = [letters, special_character]
        elif seed_code == 6:
            seed = [numbers, special_character]
        else:
            seed = [letters, numbers, special_character]

        int_reg = re.compile('^[-+]?[0-9]+$')
        is_number = int_reg.match(str(length_text))

        def generate_string(length):
            box = []
            for l_index in range(length):
                which = random.choice(seed)
                box.append(random.choice(which))
            the_string = ''.join(box)
            return the_string

        if is_number:
            length_text_to_number = int(length_text)

            if length_text_to_number < 1:
                valid = ''
                str_length = 1
                str_text = generate_string(length_text_to_number)
                invalid = [{
                    'content': str_text,
                    'describe': describe_template.format(describe=str_length)
                }]
            else:
                left_str_length = min_length - 1
                right_str_length = max_length + 1

                left_str = generate_string(left_str_length)
                right_st = generate_string(right_str_length)
                valid = generate_string(length_text_to_number)
                invalid = [
                    {'content': left_str, 'describe': describe_template.format(
                        describe=left_str_length)},
                    {'content': right_st, 'describe': describe_template.format(
                        describe=right_str_length)}
                ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': []}  # 如果输入的不是数字，则不生成任何字符串类型的字段的用例

    def _get_int_value(self, start=MIN_LENGTH, end=MAX_LENGTH):
        if start and end:
            start = int(start)
            end = int(end)
            min = start - 1
            max = end + 1
            valid = random.randint(start, end)
            invalid = [
                {'content': min, 'describe': min},
                {'content': max, 'describe': max}
            ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': ''}

    def _get_float_value(self, start=MIN_LENGTH, end=MAX_LENGTH, precision=PRO):
        if start and end and precision:
            start = int(start)
            end = int(end)
            format_text = '{:.%sf}' % precision
            exponential = -(abs(int(precision)))
            step = pow(10, exponential)
            left_start = start - step
            right_end = end + step
            left_case = format_text.format(decimal.Decimal(str(left_start)))
            right_case = format_text.format(decimal.Decimal(str(right_end)))
            valid = format_text.format(
                decimal.Decimal(str(random.uniform(start, end))))
            invalid = [
                {'content': left_case, 'describe': left_case},
                {'content': right_case, 'describe': right_case}
            ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': []}

    def _get_datatime_value(self, format_text='%s %s' % (DATE_FORMAT, TIME_FORMAT)):
        format_list = [
            {'name': '%a', 'explanation': '星期的简写。如 星期三为Web'},
            {'name': '%A', 'explanation': '星期的全写。如 星期三为Wednesday'},
            {'name': '%b', 'explanation': '月份的简写。如4月份为Apr'},
            {'name': '%B', 'explanation': '月份的全写。如4月份为April'},
            {'name': '%c', 'explanation': '日期时间的字符串表示。（如： 04/07/10 10:43:39）'},
            {'name': '%d', 'explanation': '日在这个月中的天数（是这个月的第几天）'},
            {'name': '%f', 'explanation': '微秒（范围[0,999999]）'},
            {'name': '%H', 'explanation': '小时（24小时制，[0, 23]）'},
            {'name': '%I', 'explanation': '小时（12小时制，[0, 11]）'},
            {'name': '%j', 'explanation': '日在年中的天数 [001,366]（是当年的第几天）'},
            {'name': '%m', 'explanation': '月份（[01,12]）'},
            {'name': '%M', 'explanation': '分钟（[00,59]）'},
            {'name': '%p', 'explanation': 'AM或者PM'},
            {'name': '%S', 'explanation': '秒（范围为[00,59]'},
            {'name': '%U', 'explanation': '周在当年的周数当年的第几周），星期天作为周的第一天'},
            {'name': '%w', 'explanation': '今天在这周的天数，范围为[0, 6]，6表示星期天'},
            {'name': '%W', 'explanation': '周在当年的周数（是当年的第几周），星期一作为周的第一天'},
            {'name': '%x', 'explanation': '日期字符串（如：04/07/10）'},
            {'name': '%X', 'explanation': '时间字符串（如：10:43:39）'},
            {'name': '%y', 'explanation': '2个数字表示的年份'},
            {'name': '%Y', 'explanation': '4个数字表示的年份'},
            {'name': '%z', 'explanation': '与utc时间的间隔 （如果是本地时间，返回空字符串）'},
            {'name': '%Z', 'explanation': '时区名称（如果是本地时间，返回空字符串）'},
            {'name': '%%', 'explanation': '%% => %'}
        ]
        today = datetime.datetime.today()
        format_reg = re.compile('(%[aAbBcdfHIJmMpSUwWxXyYzZ%])+')
        founds = format_reg.findall(format_text)
        if founds:
            copy_format_text = format_text
            for found_name in founds:
                for existent in format_list:
                    exist_name = existent['name']
                    if exist_name != found_name:
                        copy_format_text = copy_format_text.replace(
                            found_name, exist_name)
                        break
            if copy_format_text != format_text:
                error_format_text = copy_format_text

            else:
                error_format_text = '_' + copy_format_text + '_'
            valid = today.strftime(format_text)
            error_value = today.strftime(error_format_text)
            invalid = [
                {'content': error_value, 'describe': '错误日期格式'}
            ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': []}

    def _get_phone_value(self, phone_length=PHONE_LENGTH):
        try:
            phone_length = int(phone_length)
        except Exception as err:
            msg = '必须是数字'
            print('err: %s %s' % (err, msg))
            return {'valid': '', 'invalid': []}
        phone_length = 11
        lb = phone_length - 1
        rb = phone_length + 1
        first_number = '1'
        second_number = ['3', '5', '8']
        the_rest_number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        text = '{length}位手机号'

        def gen_mobile_phone_number(length):
            mobile_phone = []
            for index, curr_number in enumerate(range(phone_length)):
                if index == 1:
                    mobile_phone.append(first_number)
                elif index == 2:
                    mobile_phone.append(random.choice(second_number))
                else:
                    mobile_phone.append(random.choice(the_rest_number))
            mobile_phone_number = ''.join(mobile_phone)
            return mobile_phone_number

        valid = gen_mobile_phone_number(phone_length)
        lbv = gen_mobile_phone_number(lb)
        rbv = gen_mobile_phone_number(rb)
        lbd = text.format(length=lb)
        rbd = text.format(length=rb)

        invalid = [
            {'content': lbv, 'describe': lbd},
            {'content': rbv, 'describe': rbd}
        ]
        return {'valid': valid, 'invalid': invalid}

    def _get_email_value(self):
        mail_box = [
            '@126.com',
            '@sina.com',
            '@qq.com',
            '@hotmail.com',
            '@sogou.com',
            '@sohu.com',
            '@gmail.com',
            '@163.com',
            '@pingan.com.cn'
        ]
        email_at = '@'
        dot = '.'

        def gen_mailbox_header(length):
            letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'
            numbers = '0123456789'
            kind = 1    # 1 Pure letter , 2 Pure number, 3 Mixed
            if kind == 1:
                left_num = [letters]
            elif kind == 2:
                left_num = [numbers]
            elif kind == 3:
                left_num = [letters, numbers]
            else:
                left_num = [letters]
            header = []
            for curr in range(length):
                which = random.choice(left_num)
                header.append(random.choice(which))
            mailbox_header = ''.join(header)
            return mailbox_header

        header_len = random.randint(1, 10)
        correct_suffix = random.choice(mail_box)
        header_str = gen_mailbox_header(header_len)
        valid = header_str + correct_suffix

        number_2_header_len = random.randint(1, 5)
        number_2_error_suffix = correct_suffix.strip(email_at)
        number_2_content = gen_mailbox_header(
            number_2_header_len) + number_2_error_suffix
        number_2_describe = '输入无@的格式,如：ab.com'.split(",")[0]

        number_3_content = correct_suffix
        number_3_describe = '输入@前无内容的格式,如：@b.com'.split(',')[0]

        number_4_header_len = random.randint(1, 10)
        number_4_content = gen_mailbox_header(
            number_4_header_len) + email_at
        number_4_describe = '输入@后无内容的格式,如：a@'.split(',')[0]

        number_5_content = email_at
        number_5_describe = '输入@前后均无内容的格式,如：@'.split(',')[0]

        number_6_header_len = random.randint(1, 5)
        number_6_error_suffix = correct_suffix.split(dot)[0]
        if random.randint(0, 1) == 1:
            number_6_error_suffix = number_6_error_suffix + dot
        number_6_content = gen_mailbox_header(
            number_6_header_len) + number_6_error_suffix
        number_6_describe = '输入没有域名的格式,如：a@b.,a@b'.split(',')[0]

        number_7_header_len = random.randint(1, 3)
        number_7_error_suffix = correct_suffix
        if random.randint(0, 1) == 1:
            number_7_content = gen_mailbox_header(
                number_7_header_len) + email_at + number_7_error_suffix
        else:
            number_7_content = gen_mailbox_header(
                number_7_header_len) + email_at +\
                gen_mailbox_header(number_7_header_len) +\
                number_7_error_suffix
        number_7_describe = '输入E-mail中有多个@的格式,如：a@@b.com, a@b@c.d'.split(',')[
            0]

        number_8_header_len = random.randint(1, 5)
        number_8_error_suffix = correct_suffix.split(dot)[-1]
        number_8_content = gen_mailbox_header(
            number_8_header_len) + email_at + dot + number_8_error_suffix
        number_8_describe = '输入@后面直接跟域名的格式,如a@.com'.split(',')[0]

        number_9_header_len = random.randint(1, 3)
        number_9_suffix_list = correct_suffix.split(dot)
        number_9_content = gen_mailbox_header(
            number_9_header_len) + number_9_suffix_list[0] + dot +\
            gen_mailbox_header(number_9_header_len) + dot +\
            number_9_suffix_list[1]
        number_9_describe = '输入@后面有多个分隔符的格式,如：a@b.c.d, a@b.c.d.e'.split(',')[
            0]

        number_10_header_len = random.randint(1, 5)
        number_10_suffix_list = correct_suffix.split(dot)
        header_text = []
        suffix_text = []
        for curr_i in range(number_10_header_len):
            header_text.append(
                gen_mailbox_header(number_10_header_len))
            suffix_text.append(
                gen_mailbox_header(number_10_header_len))
        suffix_text.insert(0, number_10_suffix_list[0])
        suffix_text.append(number_10_suffix_list[-1])
        number_10_content = dot.join(header_text) + dot.join(suffix_text)
        number_10_describe = '输入@前面有分隔符的格式,\
        如：a.b@c.d, a.b.c@d.e, a.b@c, a.b.c@d'.split(',')[
            0]

        invalid = [
            {"content": number_2_content, "describe": number_2_describe},
            {"content": number_3_content,
                "describe": number_3_describe},
            {"content": number_4_content,
                "describe": number_4_describe},
            {"content": number_5_content,
                "describe": number_5_describe},
            {"content": number_6_content,
                "describe": number_6_describe},
            {"content": number_7_content,
                "describe": number_7_describe},
            {"content": number_8_content,
                "describe": number_8_describe},
            {"content": number_9_content,
                "describe": number_9_describe},
            {"content": number_10_content,
                "describe": number_10_describe}
        ]
        return {'valid': valid, 'invalid': invalid}

    def _get_enum_value(self, enum_value=ENUM_VALUE):
        enum_list = enum_value.split(',')
        if enum_list:
            valid = random.choice(enum_list)
            error_text = ''.join(sorted(list('abc'), reverse=True))
            invalid = [
                {'content': error_text, 'describe': '非枚举值'}
            ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': []}

    def _get_boolean_value(self, bool_value=BOOL_VALUE):
        bool_group_list = [
            ('true', 'false'),
            ('True', 'False'),
            ('TRUE', 'FALSE')
        ]
        opposite = ''

        def gen_random_str():
            letters = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsUuVvWwXxYyZz'
            errors = []
            length = random.randint(2, 7)
            for curr in range(length):
                which = random.choice(letters)
                errors.append(which)
            errors_str = ''.join(errors)
            return errors_str

        for group in bool_group_list:
            true_str = group[0]
            false_str = group[1]
            if bool_value == true_str:
                opposite = false_str
                break
            elif bool_value == false_str:
                opposite = true_str
                break
            else:
                pass

        if opposite:
            valid = bool_value
            error_value = gen_random_str()
            invalid = [
                {'content': opposite, 'describe': opposite},
                {'content': error_value, 'describe': error_value},
            ]
            return {'valid': valid, 'invalid': invalid}
        else:
            return {'valid': '', 'invalid': []}


if __name__ == '__main__':
    req_dict = {
        'name': '登录',
        'params': [
                {"field": "usernmae", "attrs": {"type": "1",
                                                "min_length": "8", "max_length": "18"}},
                {"field": "password", "attrs": {
                    "type": "2", "start": "1", "end": "10"}},
                {"field": "d1", "attrs": {"type": "3",
                                          "start": "10", "end": "100", "precision": "2"}},
                {"field": "d2", "attrs": {"type": "4", "format": "%Y -%m-%d %H:%M:%S"}},
                {"field": "d3", "attrs": {"type": "5", "phone_length": "11"}},
                {"field": "d4", "attrs": {"type": "6", "email_length": ""}},
                {"field": "d5", "attrs": {"type": "7", "enum_value": "1,2,3,4"}},
                {"field": "d6", "attrs": {"type": "8", "boolean_value": "true"}}
        ]
    }
    tc = GenTeseCases(req_dict)
    x = tc.gen_testcases()
    print(x)
